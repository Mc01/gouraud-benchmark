package rainhead;

import rainhead.com.ui.MyWindow;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 08.06.13
 * Time: 14:38
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    private Main() {}

    public static void main(String[] args) {
        int x, y;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Specify resolution of buffer area.");

        System.out.print("X: ");
        x = scanner.nextInt();

        System.out.print("Y: ");
        y = scanner.nextInt();

        MyWindow window = new MyWindow(x, y);
    }
}
