package rainhead.com.actor;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 08.06.13
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */
public class Root {
    private int x;
    private int y;
    private int color;

    public Root(int x, int y, Color c) {
        this.x = x;
        this.y = y;
        color = c.getRGB();
    }

    public Root(Root root) {
        x = root.getX();
        y = root.getY();
        color = root.getColor();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getColor() {
        return color;
    }
}
