package rainhead.com.actor;

import rainhead.com.util.Timer;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 08.06.13
 * Time: 14:43
 * To change this template use File | Settings | File Templates.
 */
public class Triangle {
    private static final String MODE_ERROR = "Error";
    private static final String MODE_VOID = "Void";
    private static final String MODE_TOP = "Top";
    private static final String MODE_BOTTOM = "Bottom";

    private Root first;
    private Root second;
    private Root third;

    private int maxX;
    private int minX;
    private int maxY;
    private int minY;

    private Triangle[] voidTriangles;
    private BufferedImage bufferedImage;

    private String currentMode;

    public Triangle(Root f, Root s, Root t, BufferedImage bi) {
        first = f;
        second = s;
        third = t;
        bufferedImage = bi;

        if (checkErrors()) return;

        checkMode();
        calculateBounds();

        if (currentMode == MODE_VOID) {
            initVoid();
        }
        else if (currentMode == MODE_TOP || currentMode == MODE_BOTTOM){
            initTris();
        }

        //System.out.println(currentMode);
    }

    private boolean checkErrors() {
        boolean errorX = first.getX() == second.getX() && second.getX() == third.getX();
        boolean errorY = first.getY() == second.getY() && second.getY() == third.getY();
        boolean errorFS = first.getX() == second.getX() && first.getY() == second.getY();
        boolean errorFT = first.getX() == third.getX() && first.getY() == third.getY();
        boolean errorST = second.getX() == third.getX() && second.getY() == third.getY();

        if (errorX || errorY || errorFS || errorFT || errorST) {
            currentMode = MODE_ERROR;
            return true;
        }
        return false;
    }

    private void checkMode() {
        Root ghostRoot;

        if (first.getY() == second.getY()) {
            applyTopBottom();
        }
        else if (first.getY() == third.getY()) {
            ghostRoot = new Root(second);
            second = new Root(third);
            third = ghostRoot;
            applyTopBottom();
        }
        else if (second.getY() == third.getY()) {
            ghostRoot = new Root(first);
            first = new Root(third);
            third = ghostRoot;
            applyTopBottom();
        }
        else {
            sortVoid();
            currentMode = MODE_VOID;
        }
    }

    private void applyTopBottom() {
        if (first.getY() > third.getY()) currentMode = MODE_TOP;
        else currentMode = MODE_BOTTOM;
    }

    private void sortVoid() {
        Root ghostRoot;

        if (first.getY() < second.getY() && first.getY() < third.getY()) {
            if (third.getY() < second.getY()) {
                ghostRoot = new Root(second);
                second = new Root(third);
                third = ghostRoot;
            }
        }
        else if (second.getY() < first.getY() && second.getY() < third.getY()) {
            if (first.getY() < third.getY()){
                ghostRoot = new Root(first);
                first = new Root(second);
                second = ghostRoot;
            }
            else {
                ghostRoot = new Root(first);
                first = new Root(second);
                second = new Root(third);
                third = ghostRoot;
            }
        }
        else if (third.getY() < first.getY()) {
            if (first.getY() < second.getY()) {
                ghostRoot = new Root(first);
                first = new Root(third);
                third = new Root(second);
                second = ghostRoot;
            }
            else {
                ghostRoot = new Root(first);
                first = new Root(third);
                third = ghostRoot;
            }
        }
    }

    private void calculateBounds() {
        maxX = Math.max(Math.max(first.getX(), second.getX()), third.getX());
        minX = Math.min(Math.min(first.getX(), second.getX()), third.getX());
        maxY = Math.max(Math.max(first.getY(), second.getY()), third.getY());
        minY = Math.min(Math.min(first.getY(), second.getY()), third.getY());
    }

    private void initVoid() {
        int fx = (int) (first.getX() + ((double) (second.getY() - first.getY()) / (double) (third.getY() - first.getY())) * (third.getX() - first.getX()));
        Root fourth = new Root(fx, second.getY(), new Color(interpolate(first.getColor(), third.getColor(), getRatioSB(first, third, fx, second.getY()))));

        voidTriangles = new Triangle[2];
        voidTriangles[0] = new Triangle(second, fourth, first, bufferedImage);
        voidTriangles[1] = new Triangle(second, fourth, third, bufferedImage);
    }

    private void initTris() {
        double iSlopeFT = (double) (first.getX() - third.getX()) / (double) (first.getY() - third.getY());
        double iSlopeST = (double) (second.getX() - third.getX()) / (double) (second.getY() - third.getY());

        double xFT;
        double xST;

        if (currentMode == MODE_TOP) {
            xFT = third.getX();
            xST = third.getX();
        }
        else {
            xFT = first.getX();
            xST = second.getX();
        }

        Timer timer = Timer.getTimer();

        for (int i = minY; i <= maxY; i++) {
            int colorFT = interpolate(first.getColor(), third.getColor(), getRatioSB(first.getY(), third.getY(), i));
            int colorST = interpolate(second.getColor(), third.getColor(), getRatioSB(second.getY(), third.getY(), i));
            for (int j = minX; j <= maxX; j++) {
                if ((j >= xFT && j <= xST) || (j >= xST && j <= xFT)) {
                    int color = interpolate(colorFT, colorST, Math.abs(j - xFT) / Math.abs(xST - xFT));
                    bufferedImage.setRGB(j, i, color);
                    timer.currentPixels++;
                }
            }
            xFT += iSlopeFT;
            xST += iSlopeST;
        }
        timer.currentTris++;
    }

    private static double getRatioSB(Root small, Root big, double x, double y) {
        double smallX = Math.abs(small.getX() - x);
        double smallY = Math.abs(small.getY() - y);
        double bigX = Math.abs(small.getX() - big.getX());
        double bigY = Math.abs(small.getY() - big.getY());
        return Math.sqrt(smallX * smallX + smallY * smallY) / Math.sqrt( bigX * bigX + bigY * bigY);
    }

    private static double getRatioSB(double small, double big, int i) {
        double smallDiff = Math.abs(small - i);
        double bigDiff = Math.abs(small - big);
        return smallDiff / bigDiff;
    }

    private static int interpolate(int smallColor, int bigColor, double ratioSB) {
        int alpha = 0xFF000000;
        int red = (int) ((1 - ratioSB) * (smallColor&0x00FF0000) + ratioSB * (bigColor&0x00FF0000)) & 0x00FF0000;
        int green = (int) ((1 - ratioSB) * (smallColor&0x0000FF00) + ratioSB * (bigColor&0x0000FF00)) & 0x0000FF00;
        int blue = (int) ((1 - ratioSB) * (smallColor&0x000000FF) + ratioSB * (bigColor&0x000000FF)) & 0x000000FF;

        return alpha + red + green + blue;
    }
}
