package rainhead.com.ui;

import rainhead.com.actor.Root;
import rainhead.com.actor.Triangle;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 10.06.13
 * Time: 18:39
 * To change this template use File | Settings | File Templates.
 */
public class Canvas extends JPanel implements Runnable {
    private static final Color myBackground = new Color(200, 200, 200);
    private static final String PATH = "example.png";

    private BufferedImage bufferedImage;
    public boolean bRaining = false;
    public boolean bLive = false;

    public Canvas(int x, int y) {
        setBackground(myBackground);
        setPreferredSize(new Dimension(x, y));
        bufferedImage = new BufferedImage(x, y, BufferedImage.TYPE_INT_ARGB);
    }

    public void paintComponent(Graphics graphics){
        super.paintComponent(graphics);
        Graphics2D graphics2D = (Graphics2D) graphics;

        Dimension dimension = getSize();
        graphics2D.drawImage(bufferedImage,
                (dimension.width - bufferedImage.getWidth()) / 2,
                (dimension.height - bufferedImage.getHeight()) / 2, null);
    }

    private void exportImage(BufferedImage bufferedImage) {
        try {
            ImageIO.write(bufferedImage, "PNG", new File(PATH));
            System.out.println("Render complete!");
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Override
    public void run() {
        rainhead.com.util.Timer.getTimer().bRaining = bRaining;
        new Thread(rainhead.com.util.Timer.getTimer()).start();

        while (bRaining) {
            Triangle ghostTriangle = new Triangle(
                    new Root((int) (Math.random() * bufferedImage.getWidth()), (int) (Math.random() * bufferedImage.getHeight()), Color.RED),
                    new Root((int) (Math.random() * bufferedImage.getWidth()), (int) (Math.random() * bufferedImage.getHeight()), Color.GREEN),
                    new Root((int) (Math.random() * bufferedImage.getWidth()), (int) (Math.random() * bufferedImage.getHeight()), Color.BLUE),
                    bufferedImage);

            if (bLive) repaint();
        }

        rainhead.com.util.Timer.getTimer().bRaining = bRaining;
        repaint();
        exportImage(bufferedImage);
    }
}
