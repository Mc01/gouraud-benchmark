package rainhead.com.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 10.06.13
 * Time: 18:39
 * To change this template use File | Settings | File Templates.
 */
public class Controls extends JPanel implements ActionListener {
    public static final int myWidth = 100;
    public static final int myHeight = 50;

    protected static final Color myBackground = new Color(100, 100, 100);
    protected static final Color myForeground = new Color(240, 240, 240);

    public static final String MODE_START = "Start";
    public static final String MODE_STOP = "Stop";
    public static final String MODE_LIVE = "Live";

    private JButton[] jButtons;
    private JCheckBox jCheckBox;

    private rainhead.com.ui.Canvas canvas;

    public Controls(rainhead.com.ui.Canvas c) {
        super(new GridBagLayout());
        setBackground(myBackground);
        setPreferredSize(new Dimension(myWidth, myHeight));

        canvas = c;
        initialize();
    }

    private void initialize() {
        jButtons = new JButton[2];

        jButtons[0] = new JButton(MODE_START);
        jButtons[1] = new JButton(MODE_STOP);
        jCheckBox = new JCheckBox(MODE_LIVE);

        skinElements();
        addElements();
        listenElements();
    }

    void skinElements() {
        for (int i = 0; i < jButtons.length; i++){
            jButtons[i].setBackground(myBackground);
            jButtons[i].setForeground(myForeground);
        }

        jCheckBox.setBackground(myBackground);
        jCheckBox.setForeground(myForeground);
    }

    void addElements() {
        for (int i = 0; i < jButtons.length; i++){
            this.add(jButtons[i], new GridBagConstraints());
        }

        this.add(jCheckBox, new GridBagConstraints());
    }

    void listenElements() {
        for (int i = 0; i < jButtons.length; i++){
            jButtons[i].addActionListener(this);
        }

        jCheckBox.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Object source = e.getSource();

        if (source instanceof JButton) {
            if (((JButton) source).getText().equals(MODE_START)) {
                if (!canvas.bRaining) {
                    canvas.bRaining = true;
                    new Thread(canvas).start();
                }
            }
            else if (((JButton) source).getText().equals(MODE_STOP)) {
                canvas.bRaining = false;
            }
        }
        else if (source instanceof JCheckBox) {
            canvas.bLive = ((JCheckBox) source).isSelected();
        }
    }
}
