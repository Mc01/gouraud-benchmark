package rainhead.com.ui;

import javax.swing.*;
import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 10.06.13
 * Time: 18:35
 * To change this template use File | Settings | File Templates.
 */
public class MyWindow extends JFrame {
    public MyWindow(int x, int y) {
        super("Gouraud Benchmark " + x + " " + y);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        Canvas canvas = new Canvas(x, y);
        Controls controls = new Controls(canvas);

        add(canvas, BorderLayout.CENTER);
        add(controls, BorderLayout.SOUTH);

        Insets insets = getInsets();
        setMinimumSize(new Dimension(Math.max(x, Controls.myWidth) + insets.left + insets.right + 50,
                y + Controls.myHeight + insets.top + insets.bottom + 50));

        setVisible(true);
    }
}
