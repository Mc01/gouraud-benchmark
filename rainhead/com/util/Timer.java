package rainhead.com.util;

/**
 * Created with IntelliJ IDEA.
 * User: Mc
 * Date: 11.06.13
 * Time: 18:10
 * To change this template use File | Settings | File Templates.
 */
public class Timer implements Runnable{
    private static Timer timer;

    private int lastTris;
    private int lastPixels;

    public int currentTris = 0;
    public int currentPixels = 0;

    public boolean bRaining = false;

    private Timer() {
    }

    @Override
    public void run() {
        while (bRaining) {
            lastTris = currentTris;
            lastPixels = currentPixels;

            currentTris = 0;
            currentPixels = 0;
            System.out.println(lastTris + " : " + lastPixels);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public static Timer getTimer() {
        if (timer != null) return timer;
        else return timer =  new Timer();
    }
}
